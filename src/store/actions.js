import { ADD_GOOD, REMOVE_GOOD } from "../const";

export const addProductAction = (payload) => ({
  type: ADD_GOOD,
  payload,
});

export const removeProductAction = (payload) => ({
  type: REMOVE_GOOD,
  payload,
});
