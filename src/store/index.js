import { combineReducers, legacy_createStore as createStore } from "redux";
import { addReducer } from "./reducer";
import {composeWithDevTools} from 'redux-devtools-extension';

const rootReducer = combineReducers({
   addReducer,
});

export const store = createStore(rootReducer, composeWithDevTools());
