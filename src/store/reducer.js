import { ADD_GOOD, REMOVE_GOOD } from "../const";

const initialState = {
  goods: [],
};

export const addReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_GOOD:
      return { ...state, goods: [...state.goods, action.payload] };
    case REMOVE_GOOD:
      return { ...state, goods: state.goods.filter((good) => good.id !== action.payload) };
    default:
      return state;
  }
};
