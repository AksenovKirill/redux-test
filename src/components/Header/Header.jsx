import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import classes from "./Header.module.css";

const Header = () => {
  const items = useSelector((state) => state.addReducer.goods);
  return (
    <div className={classes.Header}>
      <Link to="/">Main</Link>
      <Link to="/basket">Basket</Link>
      <span>Товаров: {items.length}</span>
    </div>
  );
};

export default Header;
