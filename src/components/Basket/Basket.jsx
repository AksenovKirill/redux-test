import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {removeProductAction} from "../../store/actions";
import classes from "./Basket.module.css";

const Basket = () => {
    const dispatch =  useDispatch();
    const items = useSelector((state) => state.addReducer.goods);
    console.log(items);

    const handleClickRemove = (id) => {
        dispatch(removeProductAction(id));
    };

    if(!items.length) {
        return <div className={classes.sorry}>Sorry is empty</div>
    }
// если добавил то кнопка disabled и повесить class
    //1 как понять какой клик сделан, по какому полю
    //если удалил из баскета снова снимается
  return (
    <div className={classes.wrapper}>
      <h2 className={classes.listTitle}>Goods:</h2>

        {items.map((item) => (
            <p className={classes.item} key={item.id} style={{ marginLeft: "5px" }}>
                {item.name}
                <button className={classes.buttonDelete} onClick={() => handleClickRemove(item.id)}>delete</button>
            </p>
        ))}
    </div>
  );
};

export default Basket;
