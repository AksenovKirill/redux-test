import React from "react";
import classes from "../Main/Main.module.css";
import { addProductAction } from "../../store/actions";
import { useDispatch } from "react-redux";

const products = ["samsung", "philips", "xiaomi", "apple", "hyawei", "pocophone"];

const Main = () => {
  const dispatch = useDispatch();

  const handleClickAdd = (name) => {
    const product = {
      id: Date.now(),
      name: name,
    };
    dispatch(addProductAction(product));
  };

  return (
    <div className={classes.Main}>
      {products.map((product) => (
        <p className={classes.product} key={product}>
          {product}
          <button onClick={() => handleClickAdd(product)}>Add</button>
        </p>
      ))}
    </div>
  );
};

export default Main;
//передаем флаг disabled true при пуше в сторе и отрисовываем класс disabled
//в баскет при клике на делит меняем флаг на фалс
